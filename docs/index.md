# Hunt jobs like a Munchkin

This website has been created for **computational scientists looking for a job outside academia**.
At the end of the [Do Research Like a Munchkin](https://the-munchkins.gitlab.io/do-research-like-a-munchkin/) course,
there were many questions on what is like working in the private sector, tips on how to find a job, etc.

Is this something new? Not at all! But it seems that each single PhD needs to find his way through it in the hard way.

**This project is an attempt to collect useful information**.


## Principles of this project

`Open` :material-door-open:

: This website is and will always be completely free for everyone

`Collaborative` :material-google-circles-communities:

: Everyone who profits from this website is encouraged to contribute, and leave it better than how he/she found it


## How to contribute :heartpulse:

* Give a :star:{ .heart } to our [repo](https://gitlab.com/the-munchkins/hunt-jobs-like-a-munchkin) to show your support
* See the :material-pencil:{ .heart } on the top-right of this page? It's on all pages. Use it!
* Look at the [list of topics](#what-are-we-currently-focusing-on) we are focusing on. Can you contribute? 
  Let us know by [opening a ticket](https://gitlab.com/the-munchkins/hunt-jobs-like-a-munchkin/-/issues).
* Have an idea and want to contact the maintainers of this project? [Open a ticket](https://gitlab.com/the-munchkins/hunt-jobs-like-a-munchkin/-/issues).


## What are we currently focusing on?

* How to write a good job application? See [Resume hints](resume_hints.md) and [Resume examples](resume_examples.md).
* What kind of roles / career paths are a good fit for computational scientists?

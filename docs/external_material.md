# External Material

## Job hunting and Career

### Science to Work

Peter S. Fiske is a former experimental physicist at Lawrence Livermore National Laboratory. 
He wrote the booklet 
[Putting Your Science to Work: Practical Career Strategies for Scientists](https://www.aps.org/careers/guidance/webinars/upload/Fiske-Booklet.pdf),
which is freely available and warmly recommended. 

Here you will learn to

* understand your skills and interests
* build a career plan
* write good CV, Resume, and cover letter
* prepare for job interviews

If you want to read more from Fiske, consider the books

* [To Boldly Go: A Practical Career Guide for Scientists](https://www.amazon.com/Boldly-Go-Practical-Career-Scientists/dp/0875908896)
* [Put Your Science to Work: The Take-Charge Career Guide for Scientists](https://www.amazon.com/Put-Your-Science-Work-Take-Charge/dp/0875902952)



## Data Science path

### Learn about the topic

The Andrew Ng's [Machine Learning course](https://www.coursera.org/specializations/machine-learning-introduction) is an 
excellent source. Andrew Ng is professor at the Stanford university, founder of Coursera (everything started with this very course),
founder of deeplearning.ai, promoter of data-centric AI, and one of the most respected voices in the field of Artificial Intelligence.

### Interview

[Cheat Sheets for Machine Learning Interview Topics](https://sites.google.com/view/datascience-cheat-sheets) contains
many technical and non-technical questions, with corresponding answers.